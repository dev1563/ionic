import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HederComponent } from './heder/heder.component';
import { IonicModule } from '@ionic/angular';



@NgModule({
  declarations: [HederComponent],
  exports: [HederComponent],
  imports: [
    CommonModule,
    IonicModule
  ]
})
export class ComponentsModule { }

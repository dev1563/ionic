import { Component, Input, } from '@angular/core';

@Component({
  selector: 'app-heder',
  templateUrl: './heder.component.html',
  styleUrls: ['./heder.component.scss'],
})
export class HederComponent  {

@Input() titulo: string = "";

}
